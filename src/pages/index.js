import React from "react"
import { graphql, Link } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

export default ({ data }) => {
  console.log('data', data)
  return(
  <Layout>
    <SEO title="Home" />
    <div>
      <h1>Markdowns</h1>  
      <h4>{data.allMarkdownRemark.totalCount}</h4>
      {
        data.allMarkdownRemark.edges.map(({node}) =>(
          <div key={node.id}>
            <span>title </span>
        <p>{node.excerpt}</p>
          </div>
        ))
      }
    </div>
    <h1>Hi people</h1>
    <p>Welcome to your new Gatsby site.</p>
    <p>Now go build something great.</p>
    <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
      <Image />
    </div>
    <Link to="/page-2/">Go to page 2</Link> <br />
    <Link to="/page-3/">Go to page 3</Link> <br />
    <Link to="/using-typescript/">Go to "Using TypeScript"</Link>
  </Layout>
)
}

// export default IndexPage

export const query = graphql`
{
  allMarkdownRemark(sort:{fields: frontmatter___title, order:DESC}) {
    edges {
      node {
        fields{
          slug
        }
      }
    }
  }
}
`
